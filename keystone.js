const { Keystone } = require('@keystonejs/keystone');
const { MongooseAdapter: Adapter } = require('@keystonejs/adapter-mongoose');
const config = require("./configs/config")
const initialiseData = require("./initial-data")

const PROJECT_NAME = 'GraphQL Demo';
const adapterConfig = { mongoUri: config.db };
expressSession = require('express-session');
const MongoStore = require('connect-mongo')(expressSession);
const secureCookie = process.env.NODE_ENV === 'production' || process.env.NODE_ENV === "PROD" // Default to true in production


console.log("");
console.log("> secureCookie: ", secureCookie)
console.log("");


const keystone = new Keystone({
    name: PROJECT_NAME,
    adapter: new Adapter(adapterConfig),
    onConnect: process.env.CREATE_TABLES !== 'true' && initialiseData,
    cookie: {
        secure: secureCookie,
        maxAge: 1000 * 60 * 60 * 24 * 30, // 30 days
        sameSite: false,
    },
    cookieSecret: "iDLKaQvs2NeMvpPiBdm4PXcps5jRNtT7LX57HdJ9S6SvGGwT",
    sessionStore: !config.isBuildStage ? new MongoStore({ url: config.db }) : null
});


module.exports = keystone