const moment = require("moment")
var qs = require('qs');


var register = function (Handlebars) {
    var helpers = {
        inc: function (value, options) {
            return parseInt(value) + 1;
        },
        math: function (lvalue, operator, rvalue, options) {
            lvalue = parseFloat(lvalue);
            rvalue = parseFloat(rvalue);

            return {
                "+": lvalue + rvalue,
                "-": lvalue - rvalue,
                "*": lvalue * rvalue,
                "/": lvalue / rvalue,
                "%": lvalue % rvalue
            }[operator];
        },
        booleanIf: function (lValue, operator, rValue, options) {
            // lvalue = parseFloat(lvalue);
            // rvalue = parseFloat(rvalue);

            return {
                "==": lValue == rValue,
                "!=": lValue != rValue,
                "===": lValue === rValue,
                "!==": lValue !== rValue,
                ">": lValue > rValue,
                ">=": lValue >= rValue,
                "<": lValue < rValue,
                "<=": lValue <= rValue,
                "not": !lValue,
            }[operator];
        },
        times: function (n, block) {
            let result = ""
            for (let i = 0; i < n; i++) {
                result += block.fn(i)
            }
            return result
        },
        range: function (n) {
            let range = []
            if (n > 0) {
                for (let i = 0; i < n; i++) {
                    range.push(i)
                }
            } else {
                n = Math.abs(n)
                for (let i = n - 1; i >= 0; i--) {
                    range.push(i)
                }

            }
            return range
        },
        dateFormat: function (date) {
            let format = "DD-MM-YYYY"
            return moment(date).format(format)
        },
        getQueryLink: function (param) {
            let query = {
                category: param.toLowerCase()
            }

            return `<a href="/blog?${qs.stringify(query)}">${param}</a>`
        },
        hookArticleTag: function (categories) {
            let hooks = ""
            
            if (categories !== undefined) {
                categories = JSON.parse(JSON.stringify(categories)) // graphql response parsing

                for (const i of categories) {
                    // normalization (remove accents) using https://stackoverflow.com/a/37511463/4413831
                    hooks += i.name.normalize("NFD").replace(/[\u0300-\u036f]/g, "") + " "
                }
            }

            return `<article class="centered ${hooks}">`
        }
    };

    if (Handlebars && typeof Handlebars.registerHelper === "function") {
        for (var prop in helpers) {
            Handlebars.registerHelper(prop, helpers[prop]);
        }
    } else {
        return helpers;
    }

};

module.exports.register = register;
module.exports.helpers = register(null); 
