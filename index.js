const dotenv = require('dotenv');
dotenv.config();
const keystone = require("./keystone")
// const { Keystone } = require('@keystonejs/keystone');
const { PasswordAuthStrategy } = require('@keystonejs/auth-password');
const { GraphQLApp } = require('@keystonejs/app-graphql');
const { AdminUIApp } = require('@keystonejs/app-admin-ui');
const { StaticApp } = require('@keystonejs/app-static')
const UserSchema = require('./models/User.js')
const TourSchema = require('./models/Article.js')
const CategorySchema = require('./models/Category.js')



keystone.createList("User", UserSchema)
keystone.createList("Article", TourSchema)
keystone.createList("Category", CategorySchema)

const authStrategy = keystone.createAuthStrategy({
    type: PasswordAuthStrategy,
    list: 'User',
});



module.exports = {
    configureExpress: require("./app"),
    keystone,
    apps: [
        new StaticApp({ path: '/', src: 'public/' }),
        new GraphQLApp(),
        new AdminUIApp({
            enableDefaultRoute: true,
            authStrategy,
        }),
    ],
};
