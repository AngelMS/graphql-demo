const DEFAULT_PORT = 3000

module.exports = {
    port: process.env.PORT || DEFAULT_PORT,
    db: process.env.MONGODB_URI || "mongodb://localhost:27017/graphql-demo",
    SECRET_TOKEN: "WQC8YYBJXxfEY72YLX1v3iAaBP1XnNjSNgXQ8Wf3wZuJRphqWsWch1bTQM3r9swV",
    isBuildStage: process.env.BUILD_STAGE || false,
}
