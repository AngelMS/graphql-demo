"use strict"

const express = require("express")
const api = express.Router()
const keystone = require("../keystone")


api.get("/", async (req, res) => {
    const MAIN_PAGE_MAX_ARTICLES = 5

    const QUERY_MAIN = `
        query {
            allArticles(where: {displayInMainPage: true}, sortBy: updatedAt_DESC, first: 5) {
                id
                title
                description
                cover {
                    publicUrl
                }
            }
        }
    `;

    const QUERY_OTHERS = `
        query fillMainPage($fill: Int) {
            allArticles(where: {displayInMainPage: false}, sortBy: updatedAt_DESC, first: $fill) {
                id
                title
                description
                cover {
                    publicUrl
                }
            }
        }
    `;


    let { data, errors } = await keystone.executeGraphQL({
        context: keystone.createContext({ skipAccessControl: true }),
        query: QUERY_MAIN
    })
    let articlesToDisplay = []

    if (data) {
        data = JSON.parse(JSON.stringify(data))
        articlesToDisplay = data.allArticles
    }
    
    if (errors) {
        console.log('** Error retrieving data: ', errors)
    }

    if (data.allArticles.length < MAIN_PAGE_MAX_ARTICLES) {
        let fill = MAIN_PAGE_MAX_ARTICLES - data.allArticles.length
        let { data: fillData, errors: fillErrors } = await keystone.executeGraphQL({
            context: keystone.createContext({ skipAccessControl: true }),
            query: QUERY_OTHERS,
            variables: { fill }
        })

        if (fillData) {
            fillData = JSON.parse(JSON.stringify(fillData))
            articlesToDisplay = articlesToDisplay.concat(fillData.allArticles)
        }

        if (fillErrors) {
            console.log('** Error retrieving data: ', fillErrors)
        }
    }

    res.render("index", {
        articles: articlesToDisplay
    })
})

api.get("/blog", async (req, res) => {
    const QUERY_ALL = `
        query {
            allCategories {
                name
            }
            allArticles(where: { public: true }, sortBy: updatedAt_DESC) {
                id
                title
                category {
                  name
                }
                description
                cover {
                    publicUrl
                }
            }
        }
    `;

    let { data, errors } = await keystone.executeGraphQL({
        context: keystone.createContext({ skipAccessControl: true }),
        query: QUERY_ALL
    })

    if (errors) {
        console.log('** Error retrieving data: ', errors)
    }

    res.render("blog", {
        categories: data.allCategories,
        articles: data.allArticles
    })
})

api.get("/entry/:id", async (req, res) => {
    let articleId = req.params.id

    const QUERY = `
        query getArticle($articleId: String) {
            Article(where: {id: $articleId}) {
                title
                createdAt
                cover {
                  publicUrl
                }
                category {
                  name
                }
                description
                content
              }
        }
    `;

    let { data, errors } = await keystone.executeGraphQL({
        context: keystone.createContext({ skipAccessControl: true }),
        query: QUERY,
        variables: { articleId }
    })

    if (errors) {
        console.log('** Error retrieving article: ', errors)
    }

    res.render("entry", {
        article: data.Article
    })
})

api.get("/other", (req, res) => {
    res.render("other")
})


module.exports = api
