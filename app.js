"use strict"

// No need to import express - keystone will use it from: "module.exports = app => {}"
// const express = require("express") 
const bodyParser = require("body-parser")
const hbs = require('express-handlebars');
// const cors = require('cors')

// const app = express()
const viewsRoutes = require('./routes/viewsRoutes');

module.exports = app => {
    console.log("> setting up express app...");

    app.use(bodyParser.urlencoded({ extended: false }))
    app.use(bodyParser.json())
    // app.use(cors())

    app.engine(".hbs", hbs({
        defaultLayout: "default",
        extname: ".hbs",
        helpers: require('./views/helpers/handlebars.js').helpers,
        partialsDir: __dirname + "/views/partials/"
        // layoutsDir: "../client/views/layouts",
    }))

    // https://github.com/keystonejs/keystone/issues/1887
    app.set('trust proxy', true);
    app.set("view engine", ".hbs")

    app.use("/", viewsRoutes)
}
