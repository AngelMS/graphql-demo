const crypto = require('crypto');
const randomString = () => crypto.randomBytes(6).hexSlice();
const LoremIpsum = require("lorem-ipsum").LoremIpsum;

module.exports = async keystone => {
    const {
        data: {
            _allUsersMeta: { count: usersCount },
        },
    } = await keystone.executeGraphQL({
        context: keystone.createContext({ skipAccessControl: true }),
        query: `query {
            _allUsersMeta {
                count
            }
        }`,
    });

    const {
        data: {
            _allCategoriesMeta: { count: categoriesCount },
        },
    } = await keystone.executeGraphQL({
        context: keystone.createContext({ skipAccessControl: true }),
        query: `query {
            _allCategoriesMeta {
                count
            }
        }`
    });

    const {
        data: {
            _allArticlesMeta: { count: articlesCount },
        },
    } = await keystone.executeGraphQL({
        context: keystone.createContext({ skipAccessControl: true }),
        query: `query {
            _allArticlesMeta {
                count
            }
        }`
    });

    console.log("");
    console.log("");
    console.log('> usersCount: ', usersCount)
    console.log('> categoriesCount: ', categoriesCount)
    console.log("> articlesCount: ", articlesCount)
    console.log("");


    if (usersCount === 0) {
        const password = randomString();
        const email = 'admin@example.com';

        let { data, errors } = await keystone.executeGraphQL({
            context: keystone.createContext({ skipAccessControl: true }),
            query: `mutation initialUser($password: String, $email: String) {
                createUser(data: {name: "Admin", email: $email, isAdmin: true, password: $password}) {
                    id
                }
            }`,
            variables: { password, email },
        });

        // let {data, errors} = await keystone.executeGraphQL({
        //     context: keystone.createContext({ skipAccessControl: true }),
        //     query: `mutation initialUser($password: String, $email: String) {
        //         createUser(data: {name: "Admin", email: $email, isAdmin: true, password: $password}) {
        //             id
        //         }
        //     }`,
        //     variables: { password, email },
        // });

        console.log("");
        console.log("");
        console.log('> data: ', data)
        console.log('> errors: ', errors)
        console.log("");

        console.log(`
            User created:
                email: ${email}
                password: ${password}
            Please change these details after initial login.
        `);
    }

    if (categoriesCount === 0) {

        const newData = [
            {
                data: {
                    name: "Innovation"
                }
            },
            {
                data: {
                    name: "Technology"
                }
            },
            {
                data: {
                    name: "Phisics"
                }
            },
            {
                data: {
                    name: "Architecture"
                }
            }
        ]



        const QUERY_CREATE_CATEGORIES = `mutation initialCategories($newData: [CategoriesCreateInput]) {
            createCategories(data: $newData) {
                name
            }
        }`



        let { data, errors } = await keystone.executeGraphQL({
            context: keystone.createContext({ skipAccessControl: true }),
            query: QUERY_CREATE_CATEGORIES,
            variables: { newData }
        })

        if (data) {
            data = JSON.parse(JSON.stringify(data))
            console.log("> Categories created: ", data)
        }

        if (errors) {
            console.log('** Error creating categories: ', errors)
        }

    }


    if (articlesCount === 0) {
        const lorem = new LoremIpsum({
            sentencesPerParagraph: {
                max: 8,
                min: 4
            },
            wordsPerSentence: {
                max: 16,
                min: 4
            }
        });

        const newData = [
            {
                data: {
                    public: true,
                    displayInMainPage: true,
                    // category: { connect: { id: "5f200299bb9703393d90ea7e" } },
                    title: "Article 1",
                    description: lorem.generateSentences(1),
                    content: lorem.generateParagraphs(2),
                }
            },
            {
                data: {
                    public: true,
                    displayInMainPage: true,
                    // category: { connect: { id: "5f200299bb9703393d90ea7e" } },
                    title: "Article 2",
                    description: lorem.generateSentences(1),
                    content: lorem.generateParagraphs(2),
                }
            },
            {
                data: {
                    public: true,
                    displayInMainPage: true,
                    // category: { connect: { id: "5f200299bb9703393d90ea7e" } },
                    title: "Article 3",
                    description: lorem.generateSentences(1),
                    content: lorem.generateParagraphs(2),
                }
            },
            {
                data: {
                    public: true,
                    displayInMainPage: true,
                    // category: { connect: { id: "5f200299bb9703393d90ea7e" } },
                    title: "Article 4",
                    description: lorem.generateSentences(1),
                    content: lorem.generateParagraphs(2),
                }
            },
            {
                data: {
                    public: true,
                    displayInMainPage: true,
                    // category: { connect: { id: "5f200299bb9703393d90ea7e" } },
                    title: "Article 5",
                    description: lorem.generateSentences(1),
                    content: lorem.generateParagraphs(2),
                }
            },
            {
                data: {
                    public: true,
                    displayInMainPage: true,
                    // category: { connect: { id: "5f200299bb9703393d90ea7e" } },
                    title: "Article 6",
                    description: lorem.generateSentences(1),
                    content: lorem.generateParagraphs(2),
                }
            },
        ]



        const QUERY_CREATE_SAMPLE_ARTICLES = `mutation sampleArticles($newData: [ArticlesCreateInput]) {
            createArticles(data: $newData) {
                title
            }
        }`



        let { data, errors } = await keystone.executeGraphQL({
            context: keystone.createContext({ skipAccessControl: true }),
            query: QUERY_CREATE_SAMPLE_ARTICLES,
            variables: { newData }
        })

        if (data) {
            data = JSON.parse(JSON.stringify(data))
            console.log("> Categories created: ", data)
        }

        if (errors) {
            console.log('** Error creating categories: ', errors)
        }

    }
};
