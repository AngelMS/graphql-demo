

$(function () {
    
    $(".blogFilters button").click(function () {
        if (!$(this).hasClass("active")) {
            $(".blogFilters button").removeClass("active")
            $(this).toggleClass("active")
            
            filterArticles($(this))
            computeTilesLayout()
        }
    })
    
    $(".blogFilters button").hover(function () {
        $(this).closest("li").prev().find("button").toggleClass("cleanShadow")
    })
    
    initializeFilter()
    computeTilesLayout()
})

function filterArticles(filter) {
    filter = filter.data("filter").toLowerCase()
    console.log("> filter: ", filter)

    let query = {
        category: filter
    }

    
    if (filter === "all") {
        history.replaceState("", "", "blog")
        $("article").show()
    } else {
        history.replaceState("", "", "blog?" + Qs.stringify(query))
        $("article").hide()
        $("." + filter.normalize("NFD").replace(/[\u0300-\u036f]/g, "")).show()
    }

}

function initializeFilter() {
    let filter = Qs.parse(window.location.href.split("?")[1]).category
    console.log("> filter: ", filter)
    if (filter) {
        console.log($(`.blogFilters button[data-filter=${filter}]`))
        $(`.blogFilters button[data-filter=${filter}]`).click()
    }
}