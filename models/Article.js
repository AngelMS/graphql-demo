const { Text, Checkbox, Relationship, File } = require('@keystonejs/fields');
const { LocalFileAdapter } = require('@keystonejs/file-adapters')
const { Wysiwyg } = require('@keystonejs/fields-wysiwyg-tinymce');
const { atTracking } = require('@keystonejs/list-plugins');

const basePath = process.env.NODE_ENV === 'production' ? "dist/" : ""

const PUBLIC_COVER_DIR = basePath + "public/cover"
const DATE_FORMAT = "dd-MM-yyyy - HH:mm"

const coverFileAdapter = new LocalFileAdapter({
    src: PUBLIC_COVER_DIR,
    path: "/cover"
});


// Access control functions
const userIsAdmin = ({ authentication: { item: user } }) => Boolean(user && user.isAdmin);
const userOwnsItem = ({ authentication: { item: user } }) => {
    if (!user) {
        return false;
    }

    // Instead of a boolean, you can return a GraphQL query:
    // https://www.keystonejs.com/api/access-control#graphqlwhere
    return { id: user.id };
};

const userIsAdminOrOwner = auth => {
    const isAdmin = access.userIsAdmin(auth);
    const isOwner = access.userOwnsItem(auth);
    return isAdmin ? isAdmin : isOwner;
};

const access = { userIsAdmin, userOwnsItem, userIsAdminOrOwner };

module.exports = {
    fields: {
        public: {
            type: Checkbox
        },
        displayInMainPage: {
            type: Checkbox,
            // hooks: {
            //     beforeChange: async ({ resolvedData }) => {
            //         console.log("> resolvedData: ", resolvedData)
            //         if (displayInMainPage) {
            //             resolvedData.public = true
            //         }
            //     }
            // }
        },
        category: {
            type: Relationship,
            ref: "Category",
            many: true
        },
        title: {
            type: Text
        },
        description: {
            type: Wysiwyg
        },
        cover: {
            type: File,
            adapter: coverFileAdapter
        },
        content: {
            type: Wysiwyg
        }
    },
    plugins: [
        atTracking({
            format: DATE_FORMAT
        }),
    ],
    // List-level access controls
    access: {
        read: access.userIsAdminOrOwner,
        update: access.userIsAdminOrOwner,
        create: access.userIsAdmin,
        delete: access.userIsAdmin,
        auth: true,
    },
    labelField: 'title',
}